## Data and code associated with the study

Single-cell delineation of lineage and genetic identity in the developing mouse brain

## Authors

Rachel C. Bandler^1,2,3,\#^, Ilaria Vitali^1,\#^, Ryan N. Delgado^4,5,6,\#^, Josue S. Ibarra^1^, May C. Ho^1^, Elena Dvoretskova^1^, Paul Frazel^2^, Maesoumeh Mohammadkhani^2^, Robert Machold^2^, Shane A. Liddelow^2,7,8^, Tomasz J. Nowakowski^4,5,6^, Gord Fishell^3,9^, Christian Mayer^1^

## Affiliations

1 Max Planck Institute of Neurobiology, Martinsried, Germany.

2 NYU Neuroscience Institute, Langone Medical Center, New York, NY, USA.

3 Broad Institute, Stanley Center for Psychiatric Research, Cambridge MA, USA.

4 Department of Anatomy, University of California, San Francisco, CA, USA.

5 Department of Psychiatry, University of California, San Francisco, CA, USA.

6 Eli and Edythe Broad Center for Regeneration Medicine and Stem Cell Research, University of California, San Francisco, CA, USA.

7 Department of Neuroscience and Physiology, New York University Langone School of Medicine, New York, NY, USA.

8 Department of Ophthalmology, New York University Langone School of Medicine, New York, NY, USA.

9 Harvard Medical School, Department of Neurobiology, Boston MA, USA.

\# These authors contributed equally

Correspondence: <cmayer@neuro.mpg.de>


## Sripts
There are individual scripts for the different parts of the analysis. 
### 1. Processing Lineage Barcodes
### 2. Lineage Coupling Analysis

