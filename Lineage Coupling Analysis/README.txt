Steps for Lineage Coupling Analysis:
1- Prepare the input for the Lineage Coupling Analysis by running the code in generate_input_for_lineage_coupling_analysis_TIS2_refined_clus.R with desired parameters.
2- Run lineage_coupling_analysis_wagner_way_5.py with desired parameters. For generating the commands see relevant cell in Plot_heatmaps_and_other_helper_methods.ipynb. For displaying an overview of the parameters run python lineage_coupling_analysis_wagner_way_5.py --help
3- For outputing the heatmaps with annotations, see relevant cell in Plot_heatmaps_and_other_helper_methods.ipynb.

The packages of the conda environment used to run the Python script can be found in file Requirements.txt
